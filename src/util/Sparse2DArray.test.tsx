import Sparse2DArray from "./Sparse2DArray";

const array = new Sparse2DArray<boolean>(2 ** 64, 2 ** 64);

test("dimension metadata is saved", () => {
  expect(array.width).toBe(2 ** 64);
  expect(array.height).toBe(2 ** 64);
});

test("sets/gets the value at 1,1", () => {
  array.set(1, 1, true);
  expect(array.get(1, 1)).toBe(true);
});

test("delete the value at 1,1", () => {
  array.delete(1, 1);
  expect(array.get(1, 1)).toBe(undefined);
  expect(Array.from(array.keys()).length).toBe(0);
});

test("support large coordinates", () => {
  array.set(2 ** 64, 2 ** 64, true);
  array.set(2 ** 64, 0, true);
  array.set(0, 2 ** 64, true);

  expect(array.get(2 ** 64, 2 ** 64)).toBe(true);
  expect(array.get(2 ** 64, 0)).toBe(true);
  expect(array.get(0, 2 ** 64)).toBe(true);

  expect(Array.from(array.keys()).length).toBe(3);

  array.delete(2 ** 64, 0);
  array.delete(0, 2 ** 64);

  expect(array.get(2 ** 64, 0)).toBe(undefined);
  expect(array.get(0, 2 ** 64)).toBe(undefined);
  expect(Array.from(array.keys()).length).toBe(1);
});

test("converts the key to coordinates properly", () => {
  //There's only one value left in the array and we know the value so this is fine.
  for (let val of array.keys()) {
    expect(array.convertKeyToCoordinatesArray(val)).toEqual([2 ** 64, 2 ** 64]);
  }
});
