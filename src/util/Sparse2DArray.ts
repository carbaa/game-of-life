export default class Sparse2DArray<V> {
  _map: Map<string, V>;
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this._map = new Map<string, V>();
    this.width = width;
    this.height = height;
  }

  _convertCoordinatesToKey(x: number, y: number) {
    return `${x}, ${y}`;
  }

  serialize() {
    const { _map, width, height } = this;

    return JSON.stringify({
      width,
      height,
      entries: [..._map.entries()],
    });
  }

  deserialize(json: string) {
    try {
      const obj = JSON.parse(json);
      const { width, height, entries } = obj;
      this._map = new Map(entries);
      this.width = width;
      this.height = height;
    } catch {
      console.log("error parsing");
    }
  }

  get(x: number, y: number) {
    const key = this._convertCoordinatesToKey(x, y);
    return this._map.get(key);
  }

  set(x: number, y: number, val: V) {
    const key = this._convertCoordinatesToKey(x, y);
    this._map.set(key, val);
  }

  delete(x: number, y: number) {
    const key = this._convertCoordinatesToKey(x, y);
    this._map.delete(key);
  }

  keys() {
    return this._map.keys();
  }

  convertKeyToCoordinatesArray(key: string) {
    const stringArray = key.split(",");
    return stringArray.map((str) => Number(str));
  }
}
