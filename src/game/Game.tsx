import Sparse2DArray from "../util/Sparse2DArray";

export default class Game {
  _board: Sparse2DArray<boolean>;

  constructor(width: number = 5, height: number = 5) {
    this._board = new Sparse2DArray<boolean>(width, height);
  }

  getBoard() {
    return this._board;
  }

  setBoard(board: Sparse2DArray<boolean>) {
    this._board = board;
  }

  serialize() {
    return this._board.serialize();
  }

  deserialize(json: string) {
    this._board.deserialize(json);
  }

  advance() {
    const width = this._board.width;
    const height = this._board.height;

    // Make a new board for the next transition
    const nextBoard = new Sparse2DArray<boolean>(width, height);

    // Keep a list of dead cells to be visited to prevent visiting again
    const reincarnationList = new Sparse2DArray<boolean>(width, height);

    // Define a helper function that checks whether this coordinate is valid
    const checkBoundary = (x: number, y: number) => {
      return (
        x >= 0 && x < this._board.width && y >= 0 && y < this._board.height
      );
    };

    // First check the live cells
    for (let key of this._board.keys()) {
      let neighborCount = 0;
      const [x, y] = this._board.convertKeyToCoordinatesArray(key);

      // Check the neighbors
      for (let xOffset = -1; xOffset <= 1; xOffset++) {
        for (let yOffset = -1; yOffset <= 1; yOffset++) {
          const newX = x + xOffset;
          const newY = y + yOffset;

          if ((xOffset === 0 && yOffset === 0) || !checkBoundary(newX, newY))
            continue;

          const isNeighborAlive = this._board.get(newX, newY);

          if (isNeighborAlive) neighborCount++;
          // If the cell is dead and haven't visited yet, add to the reincarnation list.
          else if (!reincarnationList.get(newX, newY)) {
            reincarnationList.set(newX, newY, true);
          }
        }
      }

      // Determine whether this cell lives next or not
      if (neighborCount === 3 || neighborCount === 2) nextBoard.set(x, y, true);
    }

    //TODO: DRY this code
    for (let key of reincarnationList.keys()) {
      let neighborCount = 0;
      const [x, y] = this._board.convertKeyToCoordinatesArray(key);

      // Check the neighbors
      for (let xOffset = -1; xOffset <= 1; xOffset++) {
        for (let yOffset = -1; yOffset <= 1; yOffset++) {
          const newX = x + xOffset;
          const newY = y + yOffset;

          if ((xOffset === 0 && yOffset === 0) || !checkBoundary(newX, newY))
            continue;

          const isNeighborAlive = this._board.get(newX, newY);
          if (isNeighborAlive) neighborCount++;
        }
      }
      if (neighborCount === 3) nextBoard.set(x, y, true);
    }

    this._board = nextBoard;
  }

  print() {
    // For quick visual debugging purposes
    console.log("___");
    for (let y = 0; y < this._board.height; y++) {
      const rowArray = [];
      for (let x = 0; x < this._board.width; x++) {
        const value = this._board.get(x, y) ? "1" : "0";
        rowArray.push(value);
      }
      console.log(rowArray.join(" "));
    }
  }
}
