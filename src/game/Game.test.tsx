import Game from "./Game";

test("Block pattern stays still", () => {
  const game = new Game(4, 4);
  const board = game.getBoard();

  board.set(1, 1, true);
  board.set(1, 2, true);
  board.set(2, 1, true);
  board.set(2, 2, true);

  game.advance();
  const nextBoard = game.getBoard();

  expect(nextBoard.get(1, 1)).toBe(true);
  expect(nextBoard.get(1, 2)).toBe(true);
  expect(nextBoard.get(2, 1)).toBe(true);
  expect(nextBoard.get(2, 2)).toBe(true);
});

test("Blinker pattern returns in two periods", () => {
  const game = new Game(5, 5);
  const board = game.getBoard();

  board.set(2, 1, true);
  board.set(2, 2, true);
  board.set(2, 3, true);

  game.advance();
  let nextBoard = game.getBoard();

  expect(nextBoard.get(1, 2)).toBe(true);
  expect(nextBoard.get(2, 2)).toBe(true);
  expect(nextBoard.get(3, 2)).toBe(true);

  game.advance();
  nextBoard = game.getBoard();

  expect(nextBoard.get(2, 1)).toBe(true);
  expect(nextBoard.get(2, 2)).toBe(true);
  expect(nextBoard.get(2, 3)).toBe(true);
});
