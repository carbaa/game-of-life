import Game from "../game/Game";

export type DisplayMode = "edit" | "play" | "stop";
export type FileMode = "new" | "save" | "load";
export type ZoomMode = "zoomin" | "zoomout";

export type ViewSettings = {
  startX: number;
  startY: number;
  viewportDimensionX: number;
  viewportDimensionY: number;
};

export type State = {
  mode: DisplayMode;
  game: null | Game;
  previousStateExists: boolean;
  viewSettings: ViewSettings;
  maxSize: number;
};

export type Action = {
  type: FileMode | DisplayMode | ZoomMode;
  payload?: any;
};

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "new":
      const game = new Game(state.maxSize, state.maxSize);
      return { ...state, mode: "edit", game };
    case "edit":
      return { ...state, mode: "edit" };
    case "play":
      return {
        ...state,
        mode: "play",
      };
    case "stop":
      return { ...state, mode: "stop" };
    case "load":
      const previousGameJSONString = localStorage.getItem("game");
      if (previousGameJSONString) {
        const previousGame = new Game(0, 0);
        previousGame.deserialize(previousGameJSONString);
        return { ...state, game: previousGame, mode: "stop" };
      } else return state;

    case "save":
      if (state.game) {
        const gameToBeSavedJSONString = state.game.serialize();
        localStorage.setItem("game", gameToBeSavedJSONString);
        return {
          ...state,
          previousStateExists: localStorage.getItem("game") ? true : false,
        };
      }
      return state;

    // For zoom actions, clip the viewport dimensions to be within 1 and max size
    case "zoomin":
      const potentialZoomedInX = state.viewSettings.viewportDimensionX / 2;
      const potentialZoomedInY = state.viewSettings.viewportDimensionY / 2;
      const ZoomedInViewSettings = {
        ...state.viewSettings,
        viewportDimensionX: potentialZoomedInX < 1 ? 1 : potentialZoomedInX,
        viewportDimensionY: potentialZoomedInY < 1 ? 1 : potentialZoomedInY,
      };
      return { ...state, viewSettings: ZoomedInViewSettings };
    case "zoomout":
      const potentialZoomedOutX = state.viewSettings.viewportDimensionX * 2;
      const potentialZoomedOutY = state.viewSettings.viewportDimensionY * 2;
      const ZoomedOutViewSettings = {
        ...state.viewSettings,
        viewportDimensionX:
          potentialZoomedOutX > state.maxSize
            ? state.maxSize
            : potentialZoomedOutX,
        viewportDimensionY:
          potentialZoomedOutY > state.maxSize
            ? state.maxSize
            : potentialZoomedOutY,
      };
      return { ...state, viewSettings: ZoomedOutViewSettings };

    default:
      return state;
  }
}
