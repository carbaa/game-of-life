import React, { useReducer } from "react";
import "./App.css";
import { State, reducer } from "./reducer/Reducer";
import Display from "./component/Display";

function App() {
  const initialState: State = {
    mode: "stop",
    game: null,
    previousStateExists: localStorage.getItem("game") ? true : false,
    viewSettings: {
      startX: 0,
      startY: 0,
      viewportDimensionX: 100,
      viewportDimensionY: 100,
    },
    maxSize: 2 ** 64,
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="App">
      <div className="container">
        <Display
          game={state.game}
          mode={state.mode}
          viewSettings={state.viewSettings}
          className="display"
        />
      </div>
      <div className="container button-container">
        <button
          onClick={() => {
            dispatch({ type: "new" });
          }}
        >
          New
        </button>
        <button
          onClick={() => {
            dispatch({ type: "edit" });
          }}
          disabled={!state.game || state.mode === "edit"}
        >
          Edit
        </button>
        <button
          onClick={() => {
            dispatch({ type: "play" });
          }}
          disabled={!state.game || state.mode === "play"}
        >
          Play
        </button>
        <button
          onClick={() => {
            dispatch({ type: "stop" });
          }}
          disabled={state.mode !== "play"}
        >
          Stop
        </button>
        <button
          onClick={() => {
            dispatch({ type: "load" });
          }}
          disabled={!state.previousStateExists}
        >
          Load
        </button>
        <button
          onClick={() => {
            dispatch({ type: "save" });
          }}
          disabled={!state.game}
        >
          Save
        </button>
        <button
          onClick={() => {
            dispatch({ type: "zoomin" });
          }}
          disabled={!state.game}
        >
          Zoom In
        </button>
        <button
          onClick={() => {
            dispatch({ type: "zoomout" });
          }}
          disabled={!state.game}
        >
          Zoom Out
        </button>
      </div>
    </div>
  );
}

export default App;
