import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("initial state", () => {
  render(<App />);
  const linkElement = screen.getByText("Initial State");
  expect(linkElement).toBeInTheDocument();
});
