import React, {
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
  MouseEvent,
} from "react";
import { DisplayMode, ViewSettings } from "../reducer/Reducer";
import Game from "../game/Game";
import Sparse2DArray from "../util/Sparse2DArray";

export default function Display({
  game,
  mode,
  className,
  viewSettings,
}: {
  game: Game | null;
  mode: DisplayMode;
  className?: string;
  viewSettings: ViewSettings;
}) {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [board, setBoard] = useState<Sparse2DArray<boolean> | null>(null);
  const animationRequestIDRef = useRef<number | null>(null);

  useEffect(() => {
    if (game) setBoard(game.getBoard());
  }, [game]);

  // Mouse handler for edit mode
  const onClick = (e: MouseEvent) => {
    if (canvasRef.current && board && mode === "edit") {
      const canvas = canvasRef.current;

      const width = canvas.clientWidth;
      const height = canvas.clientHeight;

      var rect = canvas.getBoundingClientRect();

      // Correct the mouse coordinate
      const clickedX = e.clientX - rect.left;
      const clickedY = e.clientY - rect.top;

      const { viewportDimensionX, viewportDimensionY } = viewSettings;

      const row = Math.floor((viewportDimensionX * clickedX) / width);
      const col = Math.floor((viewportDimensionY * clickedY) / height);

      const value = board.get(row, col);
      if (value) board.delete(row, col);
      else board.set(row, col, true);
      draw();
    }
  };

  const draw = () => {
    if (canvasRef.current && board) {
      const canvas = canvasRef.current;
      const ctx = canvasRef.current.getContext("2d");

      const width = canvas.width;
      const height = canvas.height;

      const { viewportDimensionX, viewportDimensionY } = viewSettings;

      if (ctx) {
        ctx.clearRect(0, 0, width, height);
        const cellWidth = width / viewportDimensionX;
        const cellHeight = height / viewportDimensionY;

        for (let key of board.keys()) {
          const [x, y] = board.convertKeyToCoordinatesArray(key);
          const canvasCoordinateX = cellWidth * x;
          const canvasCoordinateY = cellHeight * y;

          ctx.fillStyle = "green";

          // Make minimum cell size drawn to be 2x2 px
          ctx.fillRect(
            canvasCoordinateX,
            canvasCoordinateY,
            cellWidth > 2 ? cellWidth : 2,
            cellHeight > 2 ? cellHeight : 2
          );
        }

        // Draw grid
        if (cellWidth > 2 && cellHeight > 2) {
          let counter = 0;
          const selectStrokeStyle = () => {
            if (counter % 10 === 0) ctx.strokeStyle = "#ddd";
            else ctx.strokeStyle = "#666";
          };

          // draw vertically
          for (let x = 0; x < width; x += cellWidth) {
            selectStrokeStyle();
            ctx.beginPath();
            ctx.moveTo(x, 0);
            ctx.lineTo(x, height);
            ctx.stroke();
            counter++;
          }

          // draw horizontally
          counter = 0;
          for (let y = 0; y < height; y += cellHeight) {
            selectStrokeStyle();
            ctx.beginPath();
            ctx.moveTo(0, y);
            ctx.lineTo(width, y);
            ctx.stroke();
            counter++;
          }
        }
      }
    }
  };

  const matchViewportDimensionWithCanvasDimension = () => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const width = canvas.clientWidth;
      const height = canvas.clientHeight;

      // If the css size and canvas size doesn't match, change it.
      if (canvas.width !== width || canvas.height !== height) {
        canvas.width = width;
        canvas.height = height;
      }
      draw();
    }
  };

  // Drawing Effects
  useLayoutEffect(() => {
    matchViewportDimensionWithCanvasDimension();
    document.addEventListener(
      "resize",
      matchViewportDimensionWithCanvasDimension
    );
  });

  useEffect(() => {
    if (game && board) {
      if (mode === "play") {
        //if animationFrameRequestID doesn't exist, start animation
        if (!animationRequestIDRef.current) {
          const animate = () => {
            animationRequestIDRef.current = requestAnimationFrame(animate);
            game.advance();
            setBoard(game.getBoard());
          };
          animationRequestIDRef.current = requestAnimationFrame(animate);
        }
      }

      if (
        (mode === "stop" || mode === "edit") &&
        animationRequestIDRef.current
      ) {
        cancelAnimationFrame(animationRequestIDRef.current);
        animationRequestIDRef.current = null;
      }

      draw();
    }
  });

  return <canvas ref={canvasRef} className={className} onClick={onClick} />;
}
